package main
import (
    "fmt"
    "log"
    "net/http"
    "os"
)

func handler_root(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hiya")
}

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "world")
}
func main() {
    http.HandleFunc("/", handler_root)
    http.HandleFunc("/hello/", handler)
    fmt.Println("Starting Restful services...")
    fmt.Println("Using port:80")
    err := http.ListenAndServe(":80", nil)
    log.Print(err)
    errorHandler(err)
}
func errorHandler(err error){
if err!=nil {
    fmt.Println(err)
    os.Exit(1)
}
}

