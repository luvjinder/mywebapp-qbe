###################################################
#
# Please replace with your vpcid, s3 bucket name and ssl cert
#
###################################################

# AWS Global Environment
variable "name" {
  description = "the name of your stack"
  default     = "mywebapp-qbe"
}

variable "env" {
  description = "the name of your environment"
  default     = "mywebapp-qbe"
}

variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
  default     = "eu-west-1"
}

variable "vpcid" {
  description = "ID of VPC in AWS - Must change to your vpc if you would like to test terraform"
  default     = "vpc-b10c18d7"
}

# Deployment - Bucket & Beanstalk JSON

variable "mybucket" {
  description = "My s3 Bucket name"
  default     = "jin-devops"
}

variable "appjson" {
  description = "My app json filename"
  default     = "Dockerrun.aws.json.zip"
}

# AWS Credentials

variable "aws_secret_access_key" { 
default = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}

variable "aws_access_key_id" { 
default = "xxxxxxxxxxxxxxxxxxxxx"
}

# Networking & Security

variable "cidr" {
  description = "the CIDR block to provision for the VPC, if set to something other than the default, both internal_subnets and external_subnets have to be defined as well"
  default     = "10.30.0.0/16"
}

variable "internal_subnets" {
  type = "list"
  description = "a list of CIDRs for internal subnets in your VPC, must be set if the cidr variable is defined, needs to have as many elements as there are availability zones"
  default     = ["subnet-23ffb945" ,"subnet-b089d8f8", "subnet-ebbb38b1"]
}

variable "availability_zones" {
  type = "list"
  description = "a comma-separated list of availability zones, defaults to all AZ of the region, if set to something other than the defaults, both internal_subnets and external_subnets have to be defined as well"
  default     = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

variable "allowed_ips" {
  type = "list"
  default = []
}

variable "mycert" {
  description = "our ssl certificate"
  default     = "arn:aws:acm:eu-west-1:xxxxxxxxxxxxxxxx:certificate/631edea4-97d2-4a03-802a-1662e41baead"
}

variable "myservicerole" {
  description = "assumed servicerole to provide access rights to beanstalk stack"
  default     = "arn:aws:iam::xxxxxxxxxxxxxx:role/aws-service-role/elasticbeanstalk.amazonaws.com/AWSServiceRoleForElasticBeanstalk"
}
