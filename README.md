# README

## Test LIVE app

https://mywebapp-qbe.eu-west-1.elasticbeanstalk.com/hello/


## Repos

https://cloud.docker.com/repository/docker/luvjindersingh/mywebapp-qbe


https://luvjinder@bitbucket.org/luvjinder/mywebapp-qbe.git

## Required tools
1. Terraform
2. AWS CLI
3. Text editor
4. Docker

## Files
1. terraform/ - Containing all terraform files
2. Dockerfile - Build docker image to run GO webapp
3. Dockerrun.aws.json - Deployment JSON for AWS Elastic Beanstalk
4. web_api.go - Very quick & simple webapp writenn in GO

## Environment & Justification
Using Terraform to orchestrate AWS Elastic Beanstalk to rapidly build and deploy container based web app written in GO.

Terraform was used to ensure code execution can be repeatable and following infrastructure as code best practices. Data independance is achieved by keep all data away from code which would help any users trying to run the code on their own AWS accounts.

Beanstalk was used as it was a happy medium between full serverless & typical cloud based deployments. This solution provided rapid configuration & deployment ability. 

GO was used as it is a cloud centric languages and unlike languages like Python does not require software like flask/USWGI to work with http webservers. The main reason on using GO was speed of development, configuration management and deployment.

Webapp (Container) is secured by Security Groups (Firewalls), ELB (443 port only load balancer) with ssl termination as the load balanacer for performance! 

## HOW TO

1. Create an s3 bucket and create a hacky ssl cert
2. Update variables.tf with your AWS account specific information
3. Upload Dockerrun.aws.zip to your s3 Bucket
4. cd ./terraform && terraform init && terraform plan && terraform apply

## Caveats

1. No domain name registered with AWS so SSL cert had to be manually created to avoid validation process
2. SSL cert is self created and not valid (Ignore browser warning)


Thanks

Luvjinder Singh