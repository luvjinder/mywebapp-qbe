#Single main for single docker deployment to AWS Beanstalk

#Account and Object store access
terraform {
  backend "s3" {
    bucket = "jin-devops"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

#Add your access keys to ./AWS or export as env vars
provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_access_key}"
  region     = "${var.region}"
  version    = "~>1.2"
}


#Create SSL Certificate
#Manually created due to not registering a valid domain with AWS


#App deployment
resource "aws_elastic_beanstalk_application" "mywebapp-qbe" {
  name        = "mywebapp-qbe"
  description = "${var.name} beanstalk deployment"
}

resource "aws_elastic_beanstalk_application_version" "mywebapp-qbe-ver" {
  name        = "mywebapp-qbe-ver"
  application = "${aws_elastic_beanstalk_application.mywebapp-qbe.name}"
  description = "Application version created by TF"
  bucket      = "${var.mybucket}"
  key         = "${var.appjson}"
}

#ENV specifics
resource "aws_elastic_beanstalk_environment" "mywebapp-qbe-env" {
  name                = "mywebapp-qbe-env"
  application         = "${aws_elastic_beanstalk_application.mywebapp-qbe.name}"
  cname_prefix        = "${var.name}"
  solution_stack_name = "64bit Amazon Linux 2018.03 v2.12.16 running Docker 18.06.1-ce"
  version_label       = "${aws_elastic_beanstalk_application_version.mywebapp-qbe-ver.name}"
 
  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = "${var.vpcid}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "${join(",", var.internal_subnets)}"
  }
  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "CrossZone"
    value     = "true"
  }
  setting {
    namespace = "aws:elb:loadbalancer"
    name      = "SSLCertificateId"
    value     = "${var.mycert}"
  } 
 setting {
    namespace = "aws:elb:loadbalancer"
    name      = "LoadBalancerHTTPPort"
    value     = "OFF"
  } 
 setting {
    namespace = "aws:elb:loadbalancer"
    name      = "LoadBalancerHTTPSPort"
    value     = "443"
  } 
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "${var.myservicerole}"
  }
 
}
