FROM golang:latest 
RUN mkdir /webapp 
ADD . /webapp/ 
WORKDIR /webapp 
RUN go build -o main . 
EXPOSE 80
CMD ["/webapp/main"]
